package classes;

interface DisplayVariables{
	public void displayScore();
	public void displayPlayersName();
}

public class GameManager implements DisplayVariables{
	
	public String player1Name;
	public String player2Name;
	public int score;
	
	public GameManager(String player1Name,String player2Name, int score){
		this.player1Name=player1Name;
		this.player2Name=player2Name;
		this.score=score;
	}
	
	public GameManager(String player1Name, String player2Name){
		this.player1Name=player1Name;
		this.player2Name=player2Name;
	}

	@Override
	public void displayScore() {
		System.out.println("The score is: "+score);
		
	}

	@Override
	public void displayPlayersName() {
		System.out.println("Player1: "+player1Name);
		System.out.println("Player2: "+player2Name);
		
	}
	
}
